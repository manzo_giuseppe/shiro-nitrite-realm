### Copy the following in your pom.xml

    <!-- Add Manzocad repository to pom -->
    <repositories>
        <repository>
            <id>bintray-manzo-giuseppe-manzocad-maven-repository</id>
            <url>https://dl.bintray.com/manzo-giuseppe/manzocad-maven-repository</url>
        </repository>
    </repositories>

    <!-- Add dependency -->
    <dependencies>
        ...
        <dependency>
            <groupId>org.manzocad</groupId>
            <artifactId>shiro-nitrite-realm</artifactId>
            <version>1.0.0</version>
        </dependency>
        ...
    </dependencies>