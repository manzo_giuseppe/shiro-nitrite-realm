# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2017-02-08
### Changed
- Made first release version

## [0.4.1] - 2017-12-21
### Fixed
- fix(NitriteRealm): eliminate setter for Nitrite db invalid-packet 21/12/2017 11:48

## [0.4.0] - 2017-12-18
### Added
- Added methods for removing role from user

## [0.3.1] - 2017-12-18
### Fixed
- Adding role does not actually add role.

## [0.3.0] - 2017-12-13
### Added
- Added methods for add Users, Roles and Memberships

## [0.2.1] - 2017-12-13
### Added
- Customizable collection names and search field names. See [issue #1](https://bitbucket.org/manzo_giuseppe/shiro-nitrite-realm/issues/1/customs-names-for-collections-and-search)


## [0.1.0] - 2017-12-13
### Added
- [Apache Shiro](https://shiro.apache.org/) Realm Class for Authentication and Authorization
    against [Dizitart Nitrite](http://www.dizitart.org/nitrite-database.html) database
