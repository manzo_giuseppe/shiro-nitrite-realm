package org.manzocad.shiro.nitrite;

    import org.apache.shiro.authc.UnknownAccountException;
    import org.apache.shiro.authc.UsernamePasswordToken;
    import org.apache.shiro.mgt.DefaultSecurityManager;
    import org.apache.shiro.mgt.SecurityManager;
    import org.apache.shiro.subject.Subject;
    import org.assertj.core.api.SoftAssertions;
    import org.dizitart.no2.Document;
    import org.dizitart.no2.Nitrite;
    import org.dizitart.no2.filters.Filters;
    import org.junit.After;
    import org.junit.Before;
    import org.junit.Test;

    import java.util.List;

    import static org.dizitart.no2.filters.Filters.eq;
    import static org.assertj.core.api.Assertions.*;

public class NitriteRealmAmTest extends AbstractShiroTest{

    private Nitrite db;
    private SecurityManager manager;
    private NitriteRealm realm;

    @Before
    public void setUp(){
        db = Nitrite.builder().openOrCreate();
        realm = new NitriteRealm(db);
        setSecurityManager(new DefaultSecurityManager(realm));
    }

    @After
    public void tearDown(){
        clearSubject();
        db.getCollection("users").remove(Filters.ALL);
        db.getCollection("roles").remove(Filters.ALL);
    }

    @Test
    public void shouldAddUser(){
        realm.addUser("operator", "operator");
        UsernamePasswordToken token = new UsernamePasswordToken("operator","operator");
        Subject s = new Subject.Builder().buildSubject();

        Document d = realm.getDb().getCollection("users").find(eq("username", "operator")).firstOrDefault();
        List<String> roles = d.get("roles", List.class);
        s.login(token);

        SoftAssertions S = new SoftAssertions();

        S.assertThat(roles).isNotNull();
        S.assertThat(s.isAuthenticated()).isTrue();
        S.assertAll();

    }

    @Test(expected = DataConflictException.class)
    public void shouldThrowIfUserAlreadyExists(){
        realm.addUser("operator", "operator");

        realm.addUser("operator", "password");
    }


    @Test
    public void shouldChangePassword(){
        realm.addUser("operator", "operator");

        realm.passwd("operator", "newpassword");

        UsernamePasswordToken token = new UsernamePasswordToken("operator", "newpassword");
        Subject s = new Subject.Builder().buildSubject();

        s.login(token);

        assertThat(s.isAuthenticated()).isTrue();
    }

    @Test(expected = DataConflictException.class)
    public void shouldThrowIfUserDoesNotExists(){
        realm.passwd("operator", "newpassword");

        UsernamePasswordToken token = new UsernamePasswordToken("operator", "newpassword");
        Subject s = new Subject.Builder().buildSubject();

        s.login(token);
    }

    @Test
    public void shouldAddMembership(){
        realm.addUser("operator", "password");
        realm.addRole("root", "all", "rules");
        realm.addMemberShip("operator", "root");

        UsernamePasswordToken token = new UsernamePasswordToken("operator", "password");
        Subject s = new Subject.Builder().buildSubject();
        s.login(token);

        SoftAssertions S = new SoftAssertions();
        S.assertThat(s.hasRole("root")).isTrue();
        S.assertThat(s.isPermitted("rules"));
        S.assertThat(s.isPermittedAll("rules", "all"));
        S.assertAll();
    }

    @Test
    public void fix_Issue_n5(){
        realm.addRole("root", "one", "two", "three");

        Document d = realm.getDb().getCollection(realm.getRolesCollectionName()).find( Filters.eq(realm.getRoleSearchFieldName(), "root") ).firstOrDefault();

        SoftAssertions S = new SoftAssertions();
        S.assertThat(d!=null).isTrue();
        S.assertThat(d.get(realm.getRoleSearchFieldName(), String.class)).isEqualTo("root");
        S.assertAll();
    }

    @Test
    public void shouldSetPermissionsForGivenRole(){
        realm.addRole("root", "one");
        realm.setPermissions("root", "one", "two", "three");

        Document d = realm.getDb().getCollection(realm.getRolesCollectionName()).find( Filters.eq(realm.getRoleSearchFieldName(), "root") ).firstOrDefault();
        List<String> permissions = d.get("permissions", List.class);

        assertThat(permissions).contains("one", "two", "three");

    }

    @Test
    public void shouldRemoveMembership(){
        realm.addRole("removable", "one");
        realm.addUser("root", "root");
        realm.addMemberShip("root", "removable");

        realm.revokeMembership("root", "removable");

        UsernamePasswordToken token  = new UsernamePasswordToken("root", "root");
        Subject s = new Subject.Builder().buildSubject();
        s.login(token);

        assertThat(s.hasRole("removable")).isFalse();

    }

    @Test(expected = UnknownAccountException.class)
    public void shouldRemoveUser(){
        realm.addUser("root", "root");
        realm.removeUser("root");

        UsernamePasswordToken token  = new UsernamePasswordToken("root", "root");
        Subject s = new Subject.Builder().buildSubject();
        s.login(token);

    }

    @Test
    public void shouldWorkWithDefaultContructor_fix_Issue_n5(){
        NitriteRealm r = new NitriteRealm();
        r.addUser("root", "root");
    }
}
