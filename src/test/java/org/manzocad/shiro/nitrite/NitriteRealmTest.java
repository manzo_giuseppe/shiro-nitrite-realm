package org.manzocad.shiro.nitrite;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.dizitart.no2.Document;
import org.dizitart.no2.filters.Filters;
import org.dizitart.no2.Nitrite;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import org.assertj.core.api.SoftAssertions;

import java.util.Arrays;

public class NitriteRealmTest extends AbstractShiroTest{

    private Nitrite db;
    private SecurityManager manager;
    private NitriteRealm realm;

    @Before
    public void setUp(){
        db = Nitrite.builder().openOrCreate();
        realm = new NitriteRealm(db);
        setSecurityManager(new DefaultSecurityManager(realm));
    }

    @After
    public void tearDown(){
        clearSubject();
        db.getCollection("users").remove(Filters.ALL);
        db.getCollection("roles").remove(Filters.ALL);
    }

    @Test
    public void shouldAuthenticateAndAuthorize(){
        Document rootUser = new Document();
        rootUser.put("username", "root");
        rootUser.put("password", "root");
        rootUser.put("roles", Arrays.asList("admins","operators","developers"));

        db.getCollection("users").insert(rootUser);

        UsernamePasswordToken token = new UsernamePasswordToken("root","root");
        Subject s = new Subject.Builder().buildSubject();

        setSubject(s);

        try {
            s.login(token);
        }catch (AuthenticationException e){
            //DO NOTHING
        }

        SoftAssertions A = new SoftAssertions();
        A.assertThat(s.isAuthenticated()).isTrue();
        A.assertThat(s.hasAllRoles(Arrays.asList("admins","operators","developers")) ).isTrue();
        A.assertAll();

    }


    @Test
    public void shouldAuthorizeWhitPermissions(){
        Document rootUser = new Document();
        rootUser.put("username", "root");
        rootUser.put("password", "root");
        rootUser.put("roles", Arrays.asList("developers", "operators", "admins"));

        Document admins = new Document();
        admins.put("rolename", "admins");
        admins.put("permissions", Arrays.asList("all"));

        Document operators = new Document();
        operators.put("rolename", "operators");
        operators.put("permissions", Arrays.asList("edit"));

        Document developers = new Document();
        developers.put("rolename", "developers");
        developers.put("permissions", Arrays.asList("scm", "branch", "upload"));

        db.getCollection("users").insert(rootUser);
        db.getCollection("roles").insert(admins, operators, developers);

        UsernamePasswordToken token = new UsernamePasswordToken("root","root");
        Subject s = new Subject.Builder().buildSubject();

        setSubject(s);

        s.login(token);

        assertThat(s.isPermittedAll("scm", "upload", "edit", "all")).isTrue();

    }

    @Test
    public void shouldUseCustomNameForCollectionsAndFields(){
        Document rootUser = new Document();
        rootUser.put("email", "root@root");
        rootUser.put("credentials", "root");
        rootUser.put("groups", Arrays.asList("developers", "operators", "admins"));

        Document admins = new Document();
        admins.put("groupname", "admins");
        admins.put("policies", Arrays.asList("all"));

        Document operators = new Document();
        operators.put("groupname", "operators");
        operators.put("policies", Arrays.asList("edit"));

        Document developers = new Document();
        developers.put("groupname", "developers");
        developers.put("policies", Arrays.asList("scm", "branch", "upload"));

        db.getCollection("operators").insert(rootUser);
        db.getCollection("groups").insert(admins, operators, developers);

        realm.setPrincipalsCollectionName("operators");
        realm.setRolesCollectionName("groups");
        realm.setPrincipalSearchFieldName("email");
        realm.setRoleSearchFieldName("groupname");
        realm.setPasswordSearchFieldName("credentials");
        realm.setPermissionsSearchFieldName("policies");

        UsernamePasswordToken token = new UsernamePasswordToken("root@root","root");
        Subject s = new Subject.Builder().buildSubject();

        setSubject(s);

        s.login(token);

        assertThat(s.isPermittedAll("scm", "upload", "edit", "all")).isTrue();

    }

    @Test(expected = AuthenticationException.class)
    public void shouldThrowAuthenticationException(){

        UsernamePasswordToken token = new UsernamePasswordToken("root","root");
        Subject s = new Subject.Builder().buildSubject();

        setSubject(s);

        s.login(token);
    }

    @Test(expected = AuthenticationException.class)
    public void shouldThrowWhenDbIsNull(){
        NitriteRealm realm = new NitriteRealm(null);
        setSecurityManager(new DefaultSecurityManager(realm));

        UsernamePasswordToken token = new UsernamePasswordToken("root","root");
        Subject s = new Subject.Builder().buildSubject();

        setSubject(s);

        s.login(token);
    }


}
