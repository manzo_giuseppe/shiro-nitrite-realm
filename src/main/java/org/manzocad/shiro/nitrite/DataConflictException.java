package org.manzocad.shiro.nitrite;

import org.apache.shiro.dao.DataAccessException;

public class DataConflictException extends DataAccessException {
    public DataConflictException(String message) {
        super(message);
    }
}
