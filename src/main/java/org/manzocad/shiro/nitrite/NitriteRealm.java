package org.manzocad.shiro.nitrite;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.dizitart.no2.Cursor;
import org.dizitart.no2.Document;
import org.dizitart.no2.Nitrite;

import java.util.*;

import static org.dizitart.no2.filters.Filters.*;

public class NitriteRealm extends AuthorizingRealm{

    private static String DEFAULT_PRINCIPALS_COLLECTION_NAME = "users";
    private static String DEFAULT_ROLES_COLLECTION_NAME = "roles";
    private static String DEFAULT_PRINCIPAL_SEARCH_FIELD_NAME = "username";
    private static String DEFAULT_ROLE_SEARCH_FIELD_NAME = "rolename";
    private static String DEFAULT_PASSWORD_SEARCH_FIELD_NAME = "password";
    private static String DEFAULT_PERMISSIONS_SEARCH_FIELD_NAME = "permissions";

    private Nitrite db;

    private String principalsCollectionName = DEFAULT_PRINCIPALS_COLLECTION_NAME;
    private String rolesCollectionName = DEFAULT_ROLES_COLLECTION_NAME;
    private String principalSearchFieldName = DEFAULT_PRINCIPAL_SEARCH_FIELD_NAME;
    private String roleSearchFieldName = DEFAULT_ROLE_SEARCH_FIELD_NAME;
    private String passwordSearchFieldName = DEFAULT_PASSWORD_SEARCH_FIELD_NAME;
    private String permissionsSearchFieldName = DEFAULT_PERMISSIONS_SEARCH_FIELD_NAME;

    public NitriteRealm(Nitrite db) {
        setName("nitriteRealm");
        this.db = db;
        setCachingEnabled(true);
    }

    public NitriteRealm() {
        setName("nitriteRealm");
        this.db = Nitrite.builder().openOrCreate();
        setCachingEnabled(true);
    }


    public Nitrite getDb() {
        return db;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        String username = (String) principalCollection.getPrimaryPrincipal();


        Document user = db.getCollection(principalsCollectionName).find(eq(principalSearchFieldName, username)).firstOrDefault();

        Set<String> roles = new HashSet<String>( (List<String>) user.get(rolesCollectionName) );

        Set<String> permissions = new HashSet<String>();

        for(String rolename : roles){
            Cursor cursor = db.getCollection(rolesCollectionName).find(eq(roleSearchFieldName, rolename));

            Iterator<Document> it = cursor.iterator();
            while(it.hasNext()){
                Document d = it.next();
                List<String> perm = d.get(permissionsSearchFieldName, List.class);
                permissions.addAll(perm);
            }
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
        info.addStringPermissions(permissions);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        if (db == null) {
            throw new AuthenticationException("Nitrite database is null");
        }

        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;

        String uname = token.getUsername();
        String password = new String(token.getPassword());

        Document d = db.getCollection(principalsCollectionName).find(and(
            eq(principalSearchFieldName, uname),
            eq(passwordSearchFieldName, password)
        )).firstOrDefault();

        if (d == null) {
            throw new UnknownAccountException("Cannot find account name[" + uname + "]");
        }

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(uname, password, getName());

        return info;
    }

    public String getPrincipalsCollectionName() {
        return principalsCollectionName;
    }

    public void setPrincipalsCollectionName(String principalsCollectionName) {
        this.principalsCollectionName = principalsCollectionName;
    }

    public String getRolesCollectionName() {
        return rolesCollectionName;
    }

    public void setRolesCollectionName(String rolesCollectionName) {
        this.rolesCollectionName = rolesCollectionName;
    }

    public String getPrincipalSearchFieldName() {
        return principalSearchFieldName;
    }

    public void setPrincipalSearchFieldName(String principalSearchFieldName) {
        this.principalSearchFieldName = principalSearchFieldName;
    }

    public String getRoleSearchFieldName() {
        return roleSearchFieldName;
    }

    public void setRoleSearchFieldName(String roleSearchFieldName) {
        this.roleSearchFieldName = roleSearchFieldName;
    }

    public String getPasswordSearchFieldName() {
        return passwordSearchFieldName;
    }

    public void setPasswordSearchFieldName(String passwordSearchFieldName) {
        this.passwordSearchFieldName = passwordSearchFieldName;
    }

    public String getPermissionsSearchFieldName() {
        return permissionsSearchFieldName;
    }

    public void setPermissionsSearchFieldName(String permissionsSearchFieldName) {
        this.permissionsSearchFieldName = permissionsSearchFieldName;
    }

    public void addUser(String user, String password) throws DataConflictException{
        Document u = db.getCollection(principalsCollectionName).find(eq(principalSearchFieldName, user)).firstOrDefault();
        if(u != null) throw new DataConflictException("User already exists["+user+"]");

        u = new Document();
        u.put(principalSearchFieldName, user);
        u.put(passwordSearchFieldName, password);
        u.put(rolesCollectionName, new ArrayList<String>());
        db.getCollection("users").insert(u);
    }

    public void passwd(String username, String newpassword) throws DataConflictException {
        Document u = db.getCollection(principalsCollectionName).find(eq(principalSearchFieldName, username)).firstOrDefault();
        if(u == null) throw new DataConflictException("User does not exists ["+username+"]");

        u.put(passwordSearchFieldName, newpassword);

        db.getCollection(principalsCollectionName).update(eq(principalSearchFieldName, username), u);
    }

    public void addRole(String rolename, String ... permissions) throws DataConflictException{
        Document r = db.getCollection(rolesCollectionName).find(eq(roleSearchFieldName, rolename)).firstOrDefault();
        if( r != null) throw new DataConflictException("Rolename alrerady exists[" + rolename + "]");

        r = new Document();
        r.put(roleSearchFieldName, rolename);
        ArrayList<String> p = new ArrayList<String>();
        p.addAll(Arrays.asList(permissions));
        r.put(permissionsSearchFieldName, p);
        db.getCollection(rolesCollectionName).insert(r);
    }

    public void addMemberShip(String username, String rolename) throws DataConflictException{
        Document u = db.getCollection(principalsCollectionName).find(eq(principalSearchFieldName, username)).firstOrDefault();
        if(u == null) throw new DataConflictException("User does not exists ["+username+"]");

        List<String> roles = u.get(rolesCollectionName, List.class);
        if(! roles.contains(rolename)){
            roles.add(rolename);
            db.getCollection(principalsCollectionName).update(eq(principalSearchFieldName,username),u);
        }
    }

    public void setPermissions(String rolename, String ... permissions) throws DataConflictException{
        Document u = db.getCollection(rolesCollectionName).find(eq(roleSearchFieldName, rolename)).firstOrDefault();
        if(u == null) throw new DataConflictException("Role does not exists ["+rolename+"]");

        u.put(permissionsSearchFieldName, Arrays.asList(permissions));

        db.getCollection(rolesCollectionName).update(eq(roleSearchFieldName, rolename), u);
    }

    public void revokeMembership(String username, String rolename) throws DataConflictException{
        Document u = db.getCollection(principalsCollectionName).find(eq(principalSearchFieldName, username)).firstOrDefault();
        if(u == null) throw new DataConflictException("User does not exists ["+username+"]");

        List<String> roles = u.get(rolesCollectionName, List.class);

        roles.remove(rolename);

        db.getCollection(principalsCollectionName).update(eq(principalSearchFieldName,username),u);
    }

    public void removeUser(String username) {

        db.getCollection(principalsCollectionName).remove(eq(principalSearchFieldName, username));
    }
}
